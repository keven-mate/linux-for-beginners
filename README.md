# Linux For Beginners

This repo will serve as a roadmap of all the Linux things I've learned over the time I've been using.
It is labelled Linux For Beginners, but it may not serve as a manual of any kind on getting started with linux.
After all, it's based on my personal experience with Linux, though it may still work to get some people started.

The topics will be each on their own directories, which can all be found in the [/src](https://gitlab.com/keven-mate/linux-for-beginners/-/tree/main/src?ref_type=heads) directory.
In case of any issue or question, you can always open up a new issue and report it there.

PS: I'm new to Git and Giting stuff, I may do some things wrongly, don't be afraid of pointing that out to me, I'm always eager to learn, and who knows maybe it may turn into a **_Git For Beginners_**

PS2: English is not my native language, so expect some errors, I will do my best to keep them a minimum, but "There's only so much a man can do".


